import json
import dash
from dash import html
import dash_core_components as dcc
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from dash.dependencies import Input, Output

# Завантаження GeoJSON файлу
with open('ukraine.geojson') as f:
    geojson_data = json.load(f)

# Завантаження даних про міста
with open('cities.geojson') as f:
    cities_geojson_data = json.load(f)


# Завантаження даних
df = pd.read_csv('data.csv')

# Створення додатку Dash
app = dash.Dash(__name__)

# Створення розмітки додатку
app.layout = html.Div([
    html.Div([
        html.H1(children='Карта України', style={'textAlign': 'center'}),
        html.Div([
            dcc.Input(
                id='search',
                type='text',
                value='',
                placeholder='Введіть назву області',
                style={'textAlign': 'center','width': '50%', 'height': '30px', 'display': 'inline-block', 'width': '49%'}
            ),
            dcc.Input(
                id='population',
                type='text',
                value='',
                placeholder='Введіть к-сть населення',
                style={'textAlign': 'center', 'width': '50%', 'height': '30px', 'display': 'inline-block', 'width': '49%'}
            ),
        ], style={'width': '65%', 'display': 'inline-block', 'textAlign': 'center'}),
    ], style={'width': '50%', 'margin': '0 auto', 'backgroundColor': '#fff', 'padding': '20px 0'}),
    html.Div([
        dcc.Graph(
            id='map-graph',
            style={'width': '100%', 'height': '100%'},
            figure={
                    'data': [],
                    'layout': {
                        'geo': {
                            'scope': 'europe',
                            'projection': {'type': 'mercator'},
                            'showland': True,
                            'showcountries': False,
                            'landcolor': 'rgb(217, 217, 217)',
                            'subunitcolor': 'rgb(255,255,255)',
                            'countrycolor': 'rgb(255,255,255)',
                            'showlakes': False,
                            'lakecolor': 'rgb(255,255,255)',
                            'showsubunits': False,
                            'showocean': False,
                            'oceancolor': 'rgb(255,255,255)',
                            'showcoastlines': False,
                            'coastlinecolor': 'rgb(255,255,255)',
                            'showframe': False,
                            'lonaxis': {},
                            'lataxis': {}
                        },
                        'margin': {'l': 0, 'r': 0, 't': 0, 'b': 0},
                        'showlegend': False,
                        'showgrid': False,
                        'showline': False,
                        'zeroline': False,
                        'annotations': []
                    }
                }
            )

    ], style={'width': '100%', 'height': 'calc(100vh - 80px)', 'display': 'flex', 'justify-content': 'center', 'align-items': 'center', 'backgroundColor': '#fff', 'padding': '20px'})
], style={'backgroundColor': '#fff', 'opacity': '8'})

def add_city_markers(fig, cities_geojson_data):
    # Додавання маркерів міст на карту
    fig.add_trace(go.Scattermapbox(
        mode='markers',
        lon=[feature['geometry']['coordinates'][0] for feature in cities_geojson_data['features']],
        lat=[feature['geometry']['coordinates'][1] for feature in cities_geojson_data['features']],
        marker={'size': 10, 'color': 'blue'},
        text=[feature['properties']['name'] for feature in cities_geojson_data['features']],
        name = "Місто"
    ))

    return fig

@app.callback(
    Output('map-graph', 'figure'),
    Input('search', 'value'),
    Input('population', 'value')
)
def update_map(search_value, population_value):
    # Фільтрація даних на основі введеної назви області
    if population_value:
        filtered_df = df[(df['name'].str.contains(search_value, case=False)) & ((df['population'] == '') | (df['population'] >= int(population_value)))]
    else:
        filtered_df = df[(df['name'].str.contains(search_value, case=False))]

    fig = go.Figure()

    fig.add_trace(go.Choroplethmapbox(
        geojson=geojson_data,
        locations=filtered_df['name'],
        z=filtered_df['population'],
        colorscale='Viridis',
        zmin=df['population'].min(),
        zmax=df['population'].max(),
        featureidkey='properties.name',
        marker_opacity=0.5,
        showscale=False
    ))

    fig.update_layout(
        mapbox=dict(
            style='white-bg',
            zoom=5,  # Зменшуємо масштаб карти
            center=dict(lat=48.3794, lon=31.1656),
            uirevision='constant'  # Забороняємо зум карти
        ),
        margin={"r": 0, "t": 0, "l": 0, "b": 0},
        showlegend=False
    )

    fig.update_geos(
        fitbounds="locations",
        visible=False
    )

    fig = add_city_markers(fig, cities_geojson_data)

    return fig


if __name__ == '__main__':
    app.run_server(debug=True, port=8051)
